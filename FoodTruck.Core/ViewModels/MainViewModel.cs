﻿using FoodTruck.Core.Models;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.ViewModels
{
    class MainViewModel : BindableBase
    {
        private const string _ErrorMessage = "Veuillez vérifier les données saisies dans les champs";

        #region Les listes
        private ObservableCollection<Utilisateur> utilisateurs;
        public ObservableCollection<Utilisateur> Utilisateurs { get { return utilisateurs; } set { SetProperty(ref utilisateurs, value); } }
        #endregion
    }
}
