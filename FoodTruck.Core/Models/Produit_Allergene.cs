﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Produit_Allergene : BindableBase
    {
        public Produit_Allergene()
        {
        }

        public Produit_Allergene(int id_Produit, int id_Allergene)
        {
            Id_Produit = id_Produit;
            Id_Allergene = id_Allergene;
        }

        private int id_produit;

        public int Id_Produit
        {
            get => id_produit;
            set => SetProperty(ref id_produit, value);
        }

        private int id_allergene;

        public int Id_Allergene
        {
            get => id_allergene;
            set => SetProperty(ref id_allergene, value);
        }

    }
}
