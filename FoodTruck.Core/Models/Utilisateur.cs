﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Utilisateur : BindableBase
    {
        public Utilisateur()
        {
        }

        public Utilisateur(int id_Utilisateur, int id_Genre, int id_Profil, int id_Societe, string nom, string prenom, string email, DateTime date_De_Naissance, string tel)
        {
            Id_Utilisateur = id_Utilisateur;
            Id_Genre = id_Genre;
            Id_Profil = id_Profil;
            Id_Societe = id_Societe;
            Nom = nom;
            Prenom = prenom;
            Email = email;
            Date_De_Naissance = date_De_Naissance;
            Tel = tel;
        }

        private int id_utilisateur;

        public int Id_Utilisateur
        {
            get => id_utilisateur;
            set => SetProperty(ref id_utilisateur, value);
        }

        private int id_genre;

        public int Id_Genre
        {
            get => id_genre;
            set => SetProperty(ref id_genre, value);
        }

        private int id_profil;

        public int Id_Profil
        {
            get => id_profil;
            set => SetProperty(ref id_profil, value);
        }

        private int id_societe;

        public int Id_Societe
        {
            get => id_societe;
            set => SetProperty(ref id_societe, value);
        }

        private string nom;

        public string Nom
        {
            get => nom;
            set => SetProperty(ref nom, value);
        }

        private string prenom;

        public string Prenom
        {
            get => prenom;
            set => SetProperty(ref prenom, value);
        }

        private string email;

        public string Email
        {
            get => email;
            set => SetProperty(ref email, value);
        }

        private DateTime date_de_naissance;

        public DateTime Date_De_Naissance
        {
            get => date_de_naissance;
            set => SetProperty(ref date_de_naissance, value);
        }

        private string tel;

        public string Tel
        {
            get => tel;
            set => SetProperty(ref tel, value);
        }

    }
}
