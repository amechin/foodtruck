﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Commande : BindableBase
    {
        public Commande()
        {
        }

        public Commande(int id_COmmande, int id_Utilisateur, int id_Adresse, DateTime date_Commande, int id_Commande_Status)
        {
            Id_COmmande = id_COmmande;
            Id_Utilisateur = id_Utilisateur;
            Id_Adresse = id_Adresse;
            Date_Commande = date_Commande;
            Id_Commande_Status = id_Commande_Status;
        }

        private int id_commande;

        public int Id_COmmande
        {
            get => id_commande;
            set => SetProperty(ref id_commande, value);
        }


        private int id_utilisateur;

        public int Id_Utilisateur
        {
            get => id_utilisateur;
            set => SetProperty(ref id_utilisateur, value);
        }


        private int id_adresse;

        public int Id_Adresse
        {
            get => id_adresse;
            set => SetProperty(ref id_adresse, value);
        }


        private DateTime date_commande;

        public DateTime Date_Commande
        {
            get => date_commande;
            set => SetProperty(ref date_commande, value);
        }


        private int id_commande_status;

        public int Id_Commande_Status
        {
            get => id_commande_status;
            set => SetProperty(ref id_commande_status, value);
        }
    }
}
