﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Note : BindableBase
    {
        public Note()
        {
        }

        public Note(int id_Note, int id_Produit, int id_Commande, double mNote, DateTime date_Note, string description, bool actif)
        {
            Id_Note = id_Note;
            Id_Produit = id_Produit;
            Id_Commande = id_Commande;
            MNote = mNote;
            Date_Note = date_Note;
            Description = description;
            Actif = actif;
        }

        private int id_note;

        public int Id_Note
        {
            get => id_note;
            set => SetProperty(ref id_note, value);
        }

        private int id_produit;

        public int Id_Produit
        {
            get => id_produit;
            set => SetProperty(ref id_produit, value);
        }

        private int id_commande;

        public int Id_Commande
        {
            get => id_commande;
            set => SetProperty(ref id_commande, value);
        }

        private double note;

        public double MNote
        {
            get => note;
            set => SetProperty(ref note, value);
        }

        private DateTime date_note;

        public DateTime Date_Note
        {
            get => date_note;
            set => SetProperty(ref date_note, value);
        }

        private string description;

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        private bool actif;

        public bool Actif
        {
            get => actif;
            set => SetProperty(ref actif, value);
        }

    }
}
