﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Ligne_Facture : BindableBase
    {

        public Ligne_Facture()
        {
        }

        public Ligne_Facture(int id_Ligne_Commande, int id_Facture, double quantite, decimal prix_Unitaire)
        {
            Id_Ligne_Commande = id_Ligne_Commande;
            Id_Facture = id_Facture;
            Quantite = quantite;
            Prix_Unitaire = prix_Unitaire;
        }

        private int id_ligne_facture;

        public int Id_Ligne_Commande
        {
            get => id_ligne_facture;
            set => SetProperty(ref id_ligne_facture, value);
        }
        private int id_facture;

        public int Id_Facture
        {
            get => id_facture;
            set => SetProperty(ref id_facture, value);
        }

        private double quantite;

        public double Quantite
        {
            get => quantite;
            set => SetProperty(ref quantite, value);
        }

        private decimal prix_unitaire;

        public decimal Prix_Unitaire
        {
            get => prix_unitaire;
            set => SetProperty(ref prix_unitaire, value);
        }
    }
}
