﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Adresse : BindableBase
    {
        public Adresse()
        {
        }

        public Adresse(int id_Adresse, int id_Utilisateur, string libelle_Adresse, bool actif, string numero_Rue, string rue, string code_Postale, string ville, string pays)
        {
            Id_Adresse = id_Adresse;
            Id_Utilisateur = id_Utilisateur;
            Libelle_Adresse = libelle_Adresse;
            Actif = actif;
            Numero_Rue = numero_Rue;
            Rue = rue;
            Code_Postale = code_Postale;
            Ville = ville;
            Pays = pays;
        }

        private int id_adresse;

        public int Id_Adresse
        {
            get => id_adresse;
            set => SetProperty(ref id_adresse, value);
        }

        private int id_utilisateur;

        public int Id_Utilisateur
        {
            get => id_utilisateur;
            set => SetProperty(ref id_utilisateur, value);
        }

        private string libelle_adresse;

        public string Libelle_Adresse
        {
            get => libelle_adresse;
            set => SetProperty(ref libelle_adresse, value);
        }

        private bool actif;

        public bool Actif
        {
            get => actif;
            set => SetProperty(ref actif, value);
        }

        private string numero_rue;

        public string Numero_Rue
        {
            get => numero_rue;
            set => SetProperty(ref numero_rue, value);
        }

        private string rue;

        public string Rue
        {
            get => rue;
            set => SetProperty(ref rue, value);
        }

        private string code_postale;

        public string Code_Postale
        {
            get => code_postale;
            set => SetProperty(ref code_postale, value);
        }

        private string ville;

        public string Ville
        {
            get => ville;
            set => SetProperty(ref ville, value);
        }

        private string pays;

        public string Pays
        {
            get => pays;
            set => SetProperty(ref pays, value);
        }
    }
}
