﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Ligne_Commande : BindableBase
    {

        public Ligne_Commande()
        {
        }

        public Ligne_Commande(int id_Ligne_Commande, int id_Commande, int id_Produit, double qte, decimal prix_Unitaire)
        {
            Id_Ligne_Commande = id_Ligne_Commande;
            Id_Commande = id_Commande;
            Id_Produit = id_Produit;
            Qte = qte;
            Prix_Unitaire = prix_Unitaire;
        }

        private int id_ligne_commande;

        public int Id_Ligne_Commande
        {
            get => id_ligne_commande;
            set => SetProperty(ref id_ligne_commande, value);
        }

        private int id_commande;

        public int Id_Commande
        {
            get => id_commande;
            set => SetProperty(ref id_commande, value);
        }

        private int id_produit;

        public int Id_Produit
        {
            get => id_produit;
            set => SetProperty(ref id_produit, value);
        }

        private double qte;

        public double Qte
        {
            get => qte;
            set => SetProperty(ref qte, value);
        }

        private decimal prix_unitaire;

        public decimal Prix_Unitaire
        {
            get => prix_unitaire;
            set => SetProperty(ref prix_unitaire, value);
        }

    }
}
